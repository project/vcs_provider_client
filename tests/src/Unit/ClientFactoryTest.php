<?php

namespace Drupal\Tests\vcs_provider_client\Unit;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\vcs_provider_client\Client\Bitbucket;
use Drupal\vcs_provider_client\Client\Github;
use Drupal\vcs_provider_client\Client\Gitlab;
use Drupal\vcs_provider_client\ClientFactory;
use PHPUnit\Framework\TestCase;

/**
 * Class NeedsUpdateManagerTest.
 *
 * @group vcs_provider_client
 */
class ClientFactoryTest extends TestCase {

  /**
   * Test that the expected client is returned on a given URL.
   *
   * @dataProvider getUrlsAndClasses
   */
  public function testGetGithubClient($url, $class) {
    $module_handler = $this->createMock(ModuleHandlerInterface::class);
    $factory = new ClientFactory($module_handler);
    self::assertInstanceOf($class, $factory->getClientFromUrl($url));
  }

  /**
   * Data provider.
   */
  public function getUrlsAndClasses() {
    return [
      [
        'https://github.com/violinist-dev/job-summary',
        Github::class,
      ],
      [
        'https://bitbucket.org/violinist-dev/job-summary',
        Bitbucket::class,
      ],
      [
        'https://gitlab.com/violinist-dev/job-summary',
        Gitlab::class,
      ],
    ];
  }

}
