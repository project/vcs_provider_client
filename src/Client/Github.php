<?php

namespace Drupal\vcs_provider_client\Client;

use Drupal\vcs_provider_client\ClientBase;
use Github\AuthMethod;
use Github\Client;

/**
 * A github implementation of the client.
 *
 * This is the base implementation, and others should probably follow this more
 * or less.
 */
class Github extends ClientBase {

  /**
   * Github client.
   *
   * @var \Github\Client
   */
  private $client;

  /**
   * Github constructor.
   */
  public function __construct(Client $client) {
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate($token) {
    $this->client->authenticate($token, NULL, AuthMethod::ACCESS_TOKEN);
  }

  /**
   * {@inheritdoc}
   */
  public function authenticatePat($token) {
    // I think this will work?
    $this->authenticate($token);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultBranch($user, $repo) {
    /** @var \Github\Api\Repo $repo_resource */
    $repo_resource = $this->client->api('repo');
    $repo_data = $repo_resource->show($user, $repo);
    return $repo_data['default_branch'];
  }

  /**
   * {@inheritdoc}
   */
  public function getShaFromBranch($user, $repo, $branch_to_check) {
    /** @var \Github\Api\Repo $repo_resource */
    $repo_resource = $this->client->api('repo');
    $branches = $repo_resource->branches($user, $repo);
    foreach ($branches as $branch) {
      if ($branch['name'] == $branch_to_check) {
        return $branch['commit']['sha'];
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile($user, $repo, $filepath, $branch = NULL) {
    /** @var \Github\Api\Repo $repo_resource */
    $repo_resource = $this->client->api('repo');
    return $repo_resource->contents()->download($user, $repo, $filepath, $branch);
  }

  /**
   * {@inheritdoc}
   */
  public function getRepoInfo($user, $repo) {
    /** @var \Github\Api\Repo $repo_resource */
    $repo_resource = $this->client->api('repo');
    return $repo_resource->show($user, $repo);
  }

}
