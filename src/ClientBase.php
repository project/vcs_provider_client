<?php

namespace Drupal\vcs_provider_client;

/**
 * A base implementation of a client.
 */
abstract class ClientBase implements ClientInterface {

  /**
   * {@inheritdoc}
   */
  public function getDefaultBranchFromUrl($url, $user, $repo) {
    return $this->getDefaultBranch($user, $repo);
  }

  /**
   * {@inheritdoc}
   */
  public function getShaFromBranchAndUrl($url, $user, $repo, $branch) {
    return $this->getShaFromBranch($user, $repo, $branch);
  }

  /**
   * {@inheritdoc}
   */
  public function getFileFromUrl($url, $user, $repo, $filepath, $branch = NULL) {
    return $this->getFile($user, $repo, $filepath, $branch);
  }

  /**
   * {@inheritdoc}
   */
  public function getRepoInfoFromUrl($url, $user, $repo) {
    return $this->getRepoInfo($user, $repo);
  }

}
