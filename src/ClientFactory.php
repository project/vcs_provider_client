<?php

namespace Drupal\vcs_provider_client;

use Bitbucket\Client as BitbucketClient;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\vcs_provider_client\Client\Bitbucket;
use Drupal\vcs_provider_client\Client\Github;
use Drupal\vcs_provider_client\Client\Gitlab;
use Github\Client;
use Gitlab\Client as GitlabClient;

/**
 * A service to create clients from for example URL.
 */
class ClientFactory {

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * ClientFactory constructor.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * Get a client based on a URL.
   *
   * @return \Drupal\vcs_provider_client\ClientInterface
   *   A client if found.
   */
  public function getClientFromUrl($url) {
    $url = parse_url($url);
    $client = FALSE;
    switch ($url['host']) {
      case 'github.com':
        $client = new Github(new Client());
        break;

      case 'gitlab.com':
        $client = new Gitlab(new GitlabClient());
        break;

      case 'bitbucket.org':
        $client = new Bitbucket(new BitbucketClient());
        break;
    }

    // @todo Move to an event instead.
    $this->moduleHandler->alter('vcs_provider_client_from_url', $client, $url);
    return $client;
  }

}
