<?php

namespace Drupal\vcs_provider_client\Client;

use Bitbucket\Client;
use Drupal\vcs_provider_client\ClientBase;

/**
 * A bitbucket implementation.
 */
class Bitbucket extends ClientBase {

  /**
   * Github client.
   *
   * @var \Bitbucket\Client
   */
  private $client;

  /**
   * The constructor.
   */
  public function __construct(Client $client) {
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate($token) {
    $this->client->authenticate(Client::AUTH_OAUTH_TOKEN, $token);
  }

  /**
   * {@inheritdoc}
   */
  public function authenticatePat($token) {
    // I think this might work.
    $this->client->authenticate(Client::AUTH_HTTP_PASSWORD, $token);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultBranch($user, $repo) {
    $repo = $this->client->repositories()->workspaces($user)->show($repo);
    return $repo["mainbranch"]["name"];
  }

  /**
   * {@inheritdoc}
   */
  public function getShaFromBranch($user, $repo, $branch_to_check) {
    $spaces = $this->client->repositories()->workspaces($user);
    $branches = $spaces->refs($repo)->branches()->list();
    foreach ($branches["values"] as $branch) {
      if ($branch['name'] == $branch_to_check) {
        return $branch['commit']['id'];
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile($user, $repo, $filepath, $branch = NULL) {
    // In bitbucket we need a reference as well. The default branch.
    if (!$branch) {
      $branch = $this->getDefaultBranch($user, $repo);
    }
    $file = $this->client->repositories()->workspaces($user)->src($repo)->download($branch, $filepath);
    return (string) $file->getContents();
  }

  /**
   * {@inheritdoc}
   */
  public function getRepoInfo($user, $repo) {
    $info = $this->client->repositories()->workspaces($user)->show($repo);
    $slug = sprintf('%s/%s', $user, $repo);
    // Mimick this property, since this is how the github api returns it.
    $permissions = $this->client->currentUser()->listRepositoryPermissions([
      'q' => 'repository.full_name = "' . $slug . '"',
    ]);
    $info["permissions"]['admin'] = FALSE;
    foreach ($permissions["values"] as $permission_repo) {
      if ($permission_repo["repository"]["full_name"] != $slug) {
        continue;
      }
      if ($permission_repo['permission'] == 'admin') {
        $info["permissions"]['admin'] = TRUE;
        $info["permissions"]['push'] = TRUE;
      }
      if ($permission_repo['permission'] === 'write') {
        $info["permissions"]['push'] = TRUE;
      }
    }
    $info['private'] = $info['is_private'];
    return $info;
  }

}
