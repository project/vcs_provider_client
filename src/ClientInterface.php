<?php

namespace Drupal\vcs_provider_client;

/**
 * Provides an interface for clients.
 */
interface ClientInterface {

  /**
   * Authenticate against the api.
   */
  public function authenticate($token);

  /**
   * Authenticate against the API, but with a personal access token.
   */
  public function authenticatePat($token);

  /**
   * Get the default branch.
   *
   * @return string
   *   Name of branch.
   */
  public function getDefaultBranch($user, $repo);

  /**
   * Get the default branch, including using URL.
   */
  public function getDefaultBranchFromUrl($url, $user, $repo);

  /**
   * Get a sha for a branch.
   */
  public function getShaFromBranch($user, $repo, $branch);

  /**
   * Get a sha for a branch.
   */
  public function getShaFromBranchAndUrl($url, $user, $repo, $branch);

  /**
   * Get the contents of a file.
   */
  public function getFile($user, $repo, $filepath, $branch = NULL);

  /**
   * Get a file from a URL.
   */
  public function getFileFromUrl($url, $user, $repo, $filepath, $branch = NULL);

  /**
   * Get the repo info about a repo.
   */
  public function getRepoInfo($user, $repo);

  /**
   * Get the repo info about a repo.
   */
  public function getRepoInfoFromUrl($url, $user, $repo);

}
