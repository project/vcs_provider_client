<?php

namespace Drupal\vcs_provider_client\Client;

use Drupal\vcs_provider_client\ClientBase;
use Gitlab\Client;

/**
 * A Gitlab implementation of a client.
 */
class Gitlab extends ClientBase {

  /**
   * Github client.
   *
   * @var \Gitlab\Client
   */
  private $client;

  /**
   * Github constructor.
   */
  public function __construct(Client $client) {
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public function authenticate($token) {
    $this->client->authenticate($token, Client::AUTH_OAUTH_TOKEN);
  }

  /**
   * {@inheritdoc}
   */
  public function authenticatePat($token) {
    $this->client->authenticate($token, Client::AUTH_HTTP_TOKEN);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultBranch($user, $repo) {
    throw new \Exception('getDefaultBranch is not supported for gitlab.');
  }

  /**
   * {@inheritdoc}
   */
  public function getShaFromBranch($user, $repo, $branch_to_check) {
    throw new \Exception('getShaFromBranch is not supported for gitlab');
  }

  /**
   * {@inheritdoc}
   */
  public function getShaFromBranchAndUrl($url, $user, $repo, $branch) {
    $branches = $this->client->repositories()->branches($this->getProjectId($url));
    foreach ($branches as $repo_branch) {
      if ($repo_branch['name'] == $branch) {
        return $repo_branch['commit']['id'];
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFileFromUrl($url, $user, $repo, $filepath, $branch = NULL) {
    // In gitlab we need a reference as well. The default branch.
    if (!$branch) {
      $branch = $this->getDefaultBranchFromUrl($url, $user, $repo);
    }
    return $this->client->repositoryFiles()->getRawFile($this->getProjectId($url), $filepath, $branch);
  }

  /**
   * {@inheritdoc}
   */
  public function getDefaultBranchFromUrl($url, $user, $repo) {
    $repo_info = $this->client->projects()->show($this->getProjectId($url));
    return $repo_info['default_branch'];
  }

  /**
   * {@inheritdoc}
   */
  public function getRepoInfo($user, $repo) {
    throw new \Exception('getRepoInfo is not supported with gitlab.');
  }

  /**
   * {@inheritdoc}
   */
  public function getRepoInfoFromUrl($url, $user, $repo) {
    $info = $this->client->projects()->show($this->getProjectId($url));
    // Mimick this property, since this is how the github api returns it.
    $info["permissions"]['admin'] = FALSE;
    if (!empty($info["permissions"]["project_access"]["access_level"]) && $info["permissions"]["project_access"]["access_level"] >= 40) {
      $info["permissions"]['admin'] = TRUE;
      $info["permissions"]['push'] = TRUE;
    }
    if (!empty($info["permissions"]["group_access"]["access_level"]) && $info["permissions"]["group_access"]["access_level"] >= 40) {
      $info["permissions"]['admin'] = TRUE;
      $info["permissions"]['push'] = TRUE;
    }
    if ($info['visibility'] === 'private') {
      $info['private'] = TRUE;
    }
    return $info;
  }

  /**
   * The project id in gitlab.
   */
  protected function getProjectId($url) {
    $url = parse_url($url);
    return ltrim($url['path'], '/');
  }

  /**
   * {@inheritdoc}
   */
  public function getFile($user, $repo, $filepath, $branch = NULL) {
    throw new \Exception('getFile is not supported for gitlab.');
  }

}
